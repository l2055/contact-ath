package com.example;

public class ContactDuplicateException extends RuntimeException {

    public ContactDuplicateException() {
        super("Le contact existe déjà");
    }
}
